# Nuxt on AWS

This repository is a starter kit of a Nuxt application, with CI/CD configured to automatically deploy on AWS as a static website. Includes caching optimization and HTTPS setup.

## To use

1. Clone this repository and make it your own
2. Create your own GitLab CI repository
3. Add in GitLab CI settings the following CI variables:

*  `AWS_ACCESS_KEY_ID`: Your AWS access key ID
*  `AWS_SECRET_ACCESS_KEY`: The secret associated to your AWS access key
*  `AWS_DEFAULT_REGION` (optional): The AWS region you want to deploy on
*  `ROOT_DOMAIN_NAME`: The domain name you want to use for your application
*  `HOSTED_ZONE_ID`: The ID in AWS of your Route 53 hosted zone associated with the domain name

4. Push on your new repository on branch `master`
5. A new pipeline should have been triggered, and will deploy your Nuxt application!

## For more details

Check this Medium article to have a step-by-step explanation of how it works: https://durandkenny.medium.com/continuous-deployment-of-nuxt-on-aws-with-gitlab-ci-ad7f8825376e

Contact me on Twitter if you need more info: https://twitter.com/Durand_Kenny
